#!/usr/bin/env python

# Store an e-mail or a summary in the database; called by exim
# Copyright (C) 2001, 2002, 2003, 2004, 2005  Martin Michlmayr <tbm@cyrius.com>
# Copyright (C) 2006  Christoph Berg <myon@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import email, os, re, sys, time
import apt_pkg, utils, status

re_summary = re.compile(r"MIA-Summary:\s*(.+)")
re_id = re.compile(r"MIA-ID:\s*(\d+)")

apt_pkg.init()
Cnf = apt_pkg.Configuration()
apt_pkg.read_config_file_isc(Cnf, status.config)

msg = email.message_from_string(sys.stdin.read())

# Check where the mail is being delivered to because we only want to accept
# mail for mia-*@qa users.
maint = msg.get("Delivered-To", "").split("@")[0]
if not maint:
    print >> sys.stderr, "No maintainer given, please use mia-<maint>@qa"
    sys.exit(1)
# mail has to be sent to mia-*, otherwise mia-record will not accept it
if maint[:4] != "mia-":
    print >> sys.stderr, "No such user"
    sys.exit(1)
maint = maint[4:]
# make sure to not allow shell stuff, /'s, etc!
if re.compile(r"[^a-z0-9=+._-]").search(maint, re.I):
    print >> sys.stderr, "Invalid characters found in maintainer, aborting"
    sys.exit(1)

Subst = {}
Subst["__FROM__"] = "%s <%s@%s>" % (Cnf["MyAdminName"], Cnf["MyUser"], Cnf["MyHost"])
Subst["__SUBJECT__"] = msg.get("Subject", "")
Subst["__MESSAGE_ID__"] = msg.get("Message-ID", "")
Subst["__REFERENCES__"] = "%s %s" % (msg.get("References", ""), msg.get("Message-ID", ""))
Subst["__MIA_USER__"] = Cnf["MyUser"]
Subst["__MIA_DOMAIN__"] = Cnf["MyHost"]
Subst["__LOGIN__"] = maint
Subst["__EMAIL__"] = maint.replace("=", "@")

if "MIA-SUMMARY" in msg.get("Subject", ""):
    summary = id = None
    for line in msg.as_string().split("\n"):
        if not summary:
            summary = re_summary.search(line, re.I)
        if not id:
            id = re_id.search(line, re.I)

    if summary and id:
        status.write_status(maint, id.group(1), summary.group(1), sender=msg.get("From", ""))
        Subst["__SUMMARY__"] = summary.group(1)
        Subst["__ID__"] = str(id.group(1))
        mail_message = utils.TemplateSubst(Subst, Cnf["Dir::Templates"]+"/mia-record.thanks")
        utils.send_mail(mail_message)
    else:
        print >> sys.stderr, "not ok: no summary or ID"
        sys.exit(1)
else:
    try:
        mbox = open(Cnf["Dir::Database"] + "/" + maint, "a")
    except IOError, e:
        print >> sys.stderr, "Cannot open mbox file for %s to append: %s" % (maint, e)
        sys.exit(1)
    mbox.write(msg.as_string(unixfrom=1))
    mbox.close()

    date_tz = email.Utils.parsedate_tz(msg["Date"])
    when = email.Utils.mktime_tz(date_tz)


    summary = msg.get("X-MIA-Summary")
    if summary:
        summary = re.sub(r'\s*\n\s*', ' ', summary)
        status.write_status(maint, when, summary, sender=msg.get("From", ""))
        Subst["__SUMMARY__"] = summary
        mail_msg = utils.TemplateSubstMIMEMultipart(Subst, Cnf["Dir::Templates"]+"/mia-record.added")
    else:
        Subst["__ID__"] = str(when)
        Subst["__SUMMARIES__"] = ""
        for i in Cnf.subtree("Summaries").list():
            Subst["__SUMMARIES__"] += "  %s: %s\n" % (i, ", ".join(Cnf.value_list("Summaries::%s" % i)))
        mail_msg = utils.TemplateSubstMIMEMultipart(Subst, Cnf["Dir::Templates"]+"/mia-record.respond")
    # Attach the original message
    a = email.Message.Message()
    a["Content-Type"] = "message/rfc822"
    a.set_payload([msg])
    mail_msg.attach(a)
    utils.send_mail(mail_msg.as_string())

# debian.org's exim has dumb default file permissions.  Give group write perms
for file in os.listdir(Cnf["Dir::Database"]):
    try:
        os.chmod(Cnf["Dir::Database"] + "/" + file, 0664)
    except OSError:
        pass


# vim: ts=4:expandtab:shiftwidth=4:
