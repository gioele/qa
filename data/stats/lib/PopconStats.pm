# Copyright 2004 Frank Lichtenheld

package PopconStats;

use strict;
use warnings;

use lib '/srv/qa.debian.org/data/stats/lib';
use BinSrcMap;

use Exporter;

our @ISA = qw( Exporter );
our @EXPORT = qw( get_popcon_status get_popcon_status_by_bin );

our $POPCON_STATS_FILE = '/srv/qa.debian.org/data/popcon-stats.txt';
our $POPCON_STATS_OFFSET = 10;

my %popcon_stat;
my %popcon_stat_by_src;

sub init {
    
    BinSrcMap::init();
      
    open POPCON_STAT, "<", $POPCON_STATS_FILE or die "Can't open $POPCON_STATS_FILE: $!";
    
    while (<POPCON_STAT>) {
	/^#/ && next;
	/^-/ && last;

	my (undef, $pkg, $inst, $vote) = split /\s+/, $_;

	my $stat_val = $inst*0.01 + $vote*0.1 - $POPCON_STATS_OFFSET;

#	print STDERR "(PopconStats) $pkg\t=>\t$stat_val,\tI: $inst/V: $vote\n";

	$popcon_stat{$pkg} = { status => $stat_val,
			       inst => $inst,
			       vote => $vote,
			   };

	if (my $src_pkg = get_src($pkg)) {
	    $popcon_stat_by_src{$src_pkg}{status} += $stat_val;
	    $popcon_stat_by_src{$src_pkg}{inst} += $inst;
	    $popcon_stat_by_src{$src_pkg}{vote} += $vote;
	    $popcon_stat_by_src{$src_pkg}{num}++;
#	    print STDERR "(PopconStats) SRC: $src_pkg\t=>\t$popcon_stat_by_src{$src_pkg}{status}";
	} else {
	    # many, many errors since there are many unofficial
	    # and woody/sid only packages out there
#	    warn "(PopconStats) No source package found for $pkg\n";
	}
    }

    close POPCON_STAT or warn;
}

sub get_popcon_status_by_bin {
    return $popcon_stat{$_[0]}{status} || -$POPCON_STATS_OFFSET;
}

sub get_popcon_status {
    my $stat_val = $popcon_stat_by_src{$_[0]}{status} || -$POPCON_STATS_OFFSET;
    my $num = $popcon_stat_by_src{$_[0]}{num} || 1;
    return $stat_val / $num;
}

1;
