#!/usr/bin/perl

# Copyright (C) 2014-2021 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use warnings;
use Dpkg::Version;
use DBI;
use Time::HiRes qw(time);

my $logfile = "/srv/qa.debian.org/log/vcswatch.log";
my $verbose = (-t 1);
my $starttime = time;

sub logger ($)
{
	my $message = shift;
	chomp $message;
	if ($verbose) {
		print STDERR "$message\n";
	}
	open LOG, ">> $logfile";
	printf LOG "%s vcsimport[$$] %s\n", scalar(localtime), $message;
	close LOG;
}

$SIG{__WARN__} = sub {
	logger ("Warning: @_");
	return 1;
};

$SIG{__DIE__} = sub {
	logger ("Error: @_");
	return 1;
};

sub diff ($$)
{
	my ($a, $b) = @_;
	while (@$a) {
		my ($a1, $b1) = (shift @$a, shift @$b);
		next if (not defined $a1 and not defined $b1);
		return 1 if ((not defined $a1 and defined $b1) or (defined $a1 and not defined $b1));
		return 1 if ($a1 ne $b1);
	}
	return 0;
}

# read sources files

my $sources = "xzcat /srv/mirrors/debian/dists/sid/main/source/Sources.xz " .
                   "/srv/mirrors/debian/dists/experimental/main/source/Sources.xz " .
                   "/srv/mirrors/debian/dists/sid/contrib/source/Sources.xz " .
                   "/srv/mirrors/debian/dists/experimental/contrib/source/Sources.xz " .
                   "/srv/mirrors/debian/dists/sid/non-free/source/Sources.xz " .
                   "/srv/mirrors/debian/dists/experimental/non-free/source/Sources.xz |";
my $dir = "/srv/scratch/qa.debian.org/vcswatch";

my %pkg_sc = ();

my ($package, $pkgversion, $vcs, $url, $browser, $eso);
open F, $sources or die "$sources: $!";
while (<F>) {
	if (/^Package: (.+)/) {
		$package = $1;
	} elsif (/^Version: (.+)/) {
		$pkgversion = Dpkg::Version->new($1);
	} elsif (/^Vcs-Browser: (.*)/i) {
		$browser = $1;
	} elsif (/^Vcs-(.+): (.*)/i) {
		($vcs, $url) = ($1, $2);
	} elsif (/^Extra-Source-Only: yes/i) {
		$eso = 1;
	} elsif (/^$/) {
		if (($vcs or $browser) and not $eso) {
			if (not exists $pkg_sc{$package} or $pkg_sc{$package}->{package_version} < $pkgversion) {
				$pkg_sc{$package} = {
					package_version => $pkgversion,
					vcs => $vcs,
					url => $url,
					browser => $browser,
				};
			}

		}
		undef $package; undef $pkgversion; undef $vcs; undef $url; undef $browser;
		undef $eso;
	}
}
close F;

# read from database

my ($n_srcpkg, $n_new, $n_changed, $n_deleted) = (0, 0, 0, 0);

my $dbh = DBI->connect("dbi:Pg:service=qa", 'qa', '', {AutoCommit => 0, RaiseError => 1});

my $pkg_q = $dbh->prepare("SELECT package, package_version, vcs, url, browser FROM vcs");
my %pkg_db;
my $n_pkg = 0;
$pkg_q->execute;
while (my $p_db = $pkg_q->fetchrow_hashref) {
	$pkg_db{$p_db->{package}} = $p_db;
	$n_pkg++;
}

# write to database

my $pkg_insert = $dbh->prepare("INSERT INTO vcs (package, package_version, vcs, url, browser) VALUES (?, ?, ?, ?, ?)");
my $pkg_update = $dbh->prepare("UPDATE vcs SET package_version = ?, vcs = ?, url = ?, browser = ?, next_scan = now(), edited_by = NULL, edited_at = NULL WHERE package = ?");

foreach my $package (keys %pkg_sc) {
	my $p_sc = $pkg_sc{$package};
	if (my $p_db = $pkg_db{$package}) {
		if (diff ([$p_db->{package_version}],
		          [$p_sc->{package_version}])) {
			logger "$package $p_sc->{package_version} changed";
			$pkg_update->execute($p_sc->{package_version}, $p_sc->{vcs}, $p_sc->{url}, $p_sc->{browser}, $package);
			if (diff ([$p_db->{vcs}, $p_db->{url}],
				  [$p_sc->{vcs}, $p_sc->{url}])) {
				  $dbh->do("UPDATE vcs SET valid_checkout = false, dumb_http = NULL, blocked = false WHERE package = ?", undef, $package);
			}
			$n_changed++;
		}
	} else {
		logger "$package $p_sc->{package_version} is new";
		$pkg_insert->execute($package, $p_sc->{package_version}, $p_sc->{vcs}, $p_sc->{url}, $p_sc->{browser});
		$n_new++;
	}
	$n_srcpkg++;
	delete $pkg_db{$package};
}

# remove old packages

my $pkg_delete = $dbh->prepare("DELETE FROM vcs WHERE package = ?");

foreach my $package (keys %pkg_db) {
	logger "$package $pkg_db{$package}->{package_version} was removed";
	$pkg_delete->execute($package);
	$n_deleted++;

	$package =~ /^((?:lib)?.)/;
	my $pkgprefix = $1;
	my $pkgdir = "$dir/$pkgprefix/$package";
	system "rm -rf $pkgdir" if (-d $pkgdir);
}

$dbh->commit;
logger sprintf("%s packages found in database (%s in Sources files), %s new, %s changed, %s deleted, took %.1fs",
	$n_pkg, $n_srcpkg, $n_new, $n_changed, $n_deleted, time - $starttime);
