#!/usr/bin/perl

use strict;
use warnings;
use YAML::Syck qw(LoadFile);
use DB_File;

undef $/; # slurp mode
my $mulitarchhints = LoadFile("dedup.debian.net/static/multiarch-hints.yaml");

my $db_filename = 'multiarchhints-new.db';
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";

# - binary: postgresql-10-dbg
#   description: 'postgresql-10-dbg could be marked Multi-Arch: same'
#   link: https://wiki.debian.org/MultiArch/Hints#ma-same
#   severity: normal
#   source: postgresql-10
#   version: 10.0-1

foreach my $hint (@{$mulitarchhints->{hints}}) {
	my $src = $hint->{source};
	next unless ($src); # some chunks are missing "source:"
	$db{"desc:$src"} .= "$hint->{description}\n";
	$db{"link:$src"} = $hint->{link};
	$db{"ver:$src"} = $hint->{version} if ($hint->{version});
}
