## Match: regexp:[^:]*:[0-9]\+\(:[0-9]\+\)\?: warning:\/ [airp].* makes pointer from integer without a cast
## Extract: simple 0 2 ':'
## Since: 2
The build log contains the term
<pre>
warning: ... makes pointer from integer without a cast
</pre>
<p>
This usually indicates some problem in the code or some very sloppy
programming. There is some high likelihood this code might also have issues
at least on 64 bit architectures.
</p>
<p>
Using an explicit cast in the code, this compiler warning can be silenced.
</p>
<p>
If this compiler warning is emitted for the same line as a previous implicit
declaration warning, the tag <a href="E-pointer-trouble-at-implicit.html">E-pointer-trouble-at-implicit</a> is generated instead.
</p>
