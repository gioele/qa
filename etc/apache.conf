Use common-debian-service-https-redirect * qa.debian.org

<VirtualHost *:443>
    ServerName qa.debian.org

    Use common-debian-service-ssl qa.debian.org
    Use common-ssl-HSTS

    ServerAdmin debian-qa@lists.debian.org

    ErrorLog /var/log/apache2/qa.debian.org-error.log
    CustomLog /var/log/apache2/qa.debian.org-access.log privacy

    DocumentRoot /srv/qa.debian.org/web
    DirectoryIndex index.html index.php

    Alias /data /srv/qa.debian.org/data

    RewriteEngine on
    #RewriteRule ^/watch/sf.php/(.*) /watch/sf.php?project=$1
    RewriteRule ^/madison.php /cgi-bin/madison.cgi [PT]
    RewriteRule ^/data/bts/graphs/.*\.png$ /cgi-bin/bts-graphs [PT]

    FcgidIOTimeout 120
    FcgidMaxRequestLen 51200
    FcgidMaxRequestsPerProcess 500
    FcgidProcessLifeTime 1800

    ScriptAlias /cgi-bin/ /srv/qa.debian.org/cgi-bin/
    <Directory /srv/qa.debian.org/cgi-bin/>
        AllowOverride None
        Options Indexes FollowSymLinks ExecCGI
        Require all granted
    </Directory>

    <Directory /srv/qa.debian.org/web>
        Require all granted
        Options Indexes FollowSymLinks
        AddHandler fcgid-script .php
        FCGIWrapper /srv/qa.debian.org/etc/php-wrapper .php
        <Files *.php>
            Options ExecCGI
        </Files>
    </Directory>

    <Directory /srv/qa.debian.org/data>
        Require all granted
        Options Indexes FollowSymLinks
    </Directory>
    <IfModule mod_userdir.c>
        UserDir disabled
    </IfModule>

    Alias /carnivore-report /srv/qa.debian.org/carnivore/report
    <Location /carnivore-report>
        AuthType Basic
        AuthName "Carnivore Report"
        AuthUserFile /srv/qa.debian.org/etc/htpasswd
        Require user udd
    </Location>

    Alias /vcswatch.json.gz /srv/qa.debian.org/data/vcswatch/vcswatch.json.gz
    <Location /vcswatch.json.gz>
        AuthType Basic
        AuthName "vcswatch"
        AuthUserFile /srv/qa.debian.org/etc/htpasswd
        Require user udd
    </Location>

    # Use the sso.debian.org CA to validate client certificates
    # These files are kept automatically up to date.
    SSLCACertificateFile /var/lib/dsa/sso/ca.crt
    SSLCARevocationCheck chain
    SSLCARevocationFile /var/lib/dsa/sso/ca.crl

    # Broken with TLS1.3 (i.e. on buster)
    # You don't have permission to access this resource.Reason: Cannot perform Post-Handshake Authentication.
    #<Location /cgi-bin/vcswatch>
    #    # Export data about the certificate to the environment
    #    SSLOptions +StdEnvVars
    #    # Allow access if one does not have a valid certificate
    #    SSLVerifyClient optional
    #</Location>

</VirtualHost>
# vim:set syn=apache:
